package ginext

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/icentric-common/common/common"
)

func RequestIDMiddleware(c *gin.Context) {
	requestid := c.GetHeader(common.HeaderRequestID)
	if requestid == "" {
		requestid = uuid.New().String()
		c.Request.Header.Set(common.HeaderRequestID, requestid)
	}
	// set to context
	c.Set(common.HeaderRequestID, requestid)

	// set to response header as well
	c.Header(common.HeaderRequestID, requestid)

	c.Next()
}
