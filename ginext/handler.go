package ginext

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/icentric-common/common/common"
	"net/http"
)

type Request struct {
	GinCtx *gin.Context
	ctx    context.Context
}

type Response struct {
	Header http.Header `json:",omitempty"`
	*GeneralBody
}

// NewResponse makes a new response with empty body
func NewResponse() *Response {
	return &Response{
		GeneralBody: &GeneralBody{
			ResCode: http.StatusOK,
			Code:    "success",
			Message: "success",
		},
	}
}

func NewErrorResponse(err string, data interface{}) *Response {
	return &Response{
		GeneralBody: &GeneralBody{
			ResCode: http.StatusBadRequest,
			Code:    "failed",
			Message: err,
			Data:    data,
		},
	}
}

// NewResponseData makes a new response with body data
func NewResponseData(data interface{}) *Response {
	return &Response{
		GeneralBody: &GeneralBody{
			Code:    "success",
			Message: "success",
			Data:    data,
		},
	}
}

// NewResponseWithPager makes a new response with body data & pager
func NewResponseWithPager(code string, data interface{}, pager *Pager) *Response {
	return &Response{
		GeneralBody: NewBodyPaginated(

			"success",
			"success",
			data,
			pager),
	}
}

type Handler func(r *Request) *Response

// NewRequest creates a new handler request
func NewRequest(c *gin.Context) *Request {
	ctx := FromGinRequestContext(c)
	req := &Request{
		GinCtx: c,
		ctx:    ctx,
	}

	return req
}

func (r *Request) Context() context.Context {
	if r.ctx == nil {
		r.ctx = context.Background()
	}

	return r.ctx
}

func WrapHandler(handler Handler) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			resp *Response
		)

		defer func() {

			if resp == nil {
				return
			}

			for k, v := range resp.Header {
				for _, v_ := range v {
					c.Header(k, v_)
				}
			}
			c.JSON(resp.GeneralBody.ResCode, resp.GeneralBody)
		}()

		req := NewRequest(c)
		resp = handler(req)
	}
}

// MustBind does a binding on v with income request data
// it'll panic if any invalid data (and by design, it should be recovered by error handler middleware)
func (r *Request) MustBind(v interface{}) {
	r.MustNoError(r.GinCtx.ShouldBind(v))
}

func (r *Request) MustBindUri(v interface{}) {
	r.MustNoError(r.GinCtx.ShouldBindUri(v))
}

// MustNoError makes a ASSERT on err variable, panic when it's not nil
// then it must be recovered by WrapHandler
func (r *Request) MustNoError(err error) {
	if err != nil {
		panic(err)
	}
}

func (r *Request) Uint64UserID() uint64 {
	return Uint64HeaderValue(r.GinCtx, common.HeaderUserID)
}

func (r *Request) Uint64TenantID() uint64 {
	return Uint64HeaderValue(r.GinCtx, common.HeaderTenantID)
}

func (r *Request) Param(key string) string {
	return r.GinCtx.Param(key)
}

func (r *Request) Query(key string) string {
	return r.GinCtx.Query(key)
}
