package ginext

import "net/http"

// BodyMeta represents a body meta data like pagination or extra response information
// it should always be rendered as a map of key: value
type BodyMeta map[string]interface{}

// GeneralBody defines a general response body
type GeneralBody struct {
	ResCode int         `json:"res_code"`
	Code    string      `json:"code,omitempty"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
	Error   interface{} `json:"error,omitempty"`
	Meta    BodyMeta    `json:"meta,omitempty"`
}

func NewBodyPaginated(code string, message string, data interface{}, pager *Pager) *GeneralBody {
	return &GeneralBody{
		ResCode: http.StatusOK,
		Code:    code,
		Message: message,
		Data:    data,
		Meta: BodyMeta{
			"page":        pager.GetPage(),
			"total_pages": pager.GetTotalPages(),
			"page_size":   pager.GetPageSize(),
			"total":       pager.TotalRows,
			"pages":       pager.GetTotalPages(),
		},
	}
}
