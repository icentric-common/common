package common

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	gonanoid "github.com/matoous/go-nanoid/v2"
	"github.com/sendgrid/rest"
	"math/rand"
	"net/http"
	"reflect"
	"strings"
)

func RandStringBytes(n int, upper bool) string {

	var LETTER_BYTES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
	if upper {
		LETTER_BYTES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	}

	b := make([]byte, n)
	for i := range b {
		b[i] = LETTER_BYTES[rand.Intn(len(LETTER_BYTES))]
	}
	return string(b)
}

func SendRestAPI(url string, method rest.Method, header map[string]string, queryParam map[string]string, bodyInput interface{}) (body string, headers map[string][]string, err error) {
	request := rest.Request{
		Method:      method,
		BaseURL:     url,
		Headers:     header,
		QueryParams: queryParam,
	}
	if bodyInput != nil {
		bodyData, err := json.Marshal(bodyInput)
		if err != nil {
			return "", nil, err
		}
		request.Body = bodyData
	}
	response, err := rest.Send(request)
	if err != nil {
		return "", nil, err
	} else {
		if response.StatusCode != http.StatusOK &&
			response.StatusCode != http.StatusCreated &&
			response.StatusCode != http.StatusNoContent &&
			response.StatusCode != http.StatusAccepted {
			return response.Body,
				response.Headers,
				fmt.Errorf("Error when call Rest API, Status %s , Response Body %s", response.StatusCode, response.Body)
		} else {
			return response.Body, response.Headers, nil
		}
	}
}

// SliceContains return true if value exist in slice, otherwise false
func SliceContains(value, slice interface{}) bool {
	valueInterface := reflect.ValueOf(slice)
	if valueInterface.Kind() == reflect.Slice {
		for i := 0; i < valueInterface.Len(); i++ {
			if value == valueInterface.Index(i).Interface() {
				return true
			}
		}
	} else {
		return false
	}
	return false
}

func GenerateObjectKey(microservice, objectName, userKey string) (objectKey string, err error) {
	// generate nano id
	id, err := gonanoid.New()
	if err != nil {
		return "", err
	}
	// result
	result := append(make([]string, 0), microservice, objectName, id)
	if userKey != "" {
		// check valid userKey
		if strings.Contains(userKey, KeySeparator) {
			return "", fmt.Errorf(`user key cannot contain "%s" characters`, KeySeparator)
		}
		result = append(result, userKey)
	}
	objectKey = strings.Join(result, KeySeparator)
	return objectKey, nil
}

func GetObjectKey(str string) string {
	splitString := strings.Split(str, KeySeparator)
	return splitString[len(splitString)-1]
}

func RouterGroupWithObject(routerGroup *gin.RouterGroup) *gin.RouterGroup {
	return routerGroup.Group(fmt.Sprintf("/:%v", GinParamObject))
}
